// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once
#define _DETOURS2

#include "targetver.h"

#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <windows.h>

#ifdef _DETOURS2
#include "../Detours2/detours.h"
#ifdef _WIN32
#pragma comment(lib,"../Detours2/detours32.lib")
#endif
#ifdef _WIN64
#pragma comment(lib,"../Detours2/detours64.lib")
#endif
#endif

#ifdef _DETOURS3
#endif