// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//

#pragma once

#include "targetver.h"
#define _DETOURS2

#define WIN32_LEAN_AND_MEAN             //  从 Windows 头文件中排除极少使用的信息
// Windows 头文件: 
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <wchar.h>
#include <strsafe.h>

#ifdef _DETOURS2
#include "../Detours2/detours.h"
	#ifdef _WIN32
	#pragma comment(lib,"../Detours2/detours32.lib")
	#endif
	#ifdef _WIN64
	#pragma comment(lib,"../Detours2/detours64.lib")
	#endif
#endif

#ifdef _DETOURS3
#endif

#define STACK_FRAME_BEGIN __asm{\
	_asm    push ebp                \
	_asm    mov  ebp, esp            \
	_asm    sub  esp, __LOCAL_SIZE   \
	_asm    pushad                  \
}{

#define STACK_FRAME_END }\
	__asm{              \
	_asm    popad       \
	_asm    mov  esp, ebp\
	_asm    pop  ebp    \
	}

#define NAKED __declspec(naked)

#define MSGBOXBREAK(x,y) MessageBox(NULL,_T(x),_T(y),MB_OK)



// TODO:  在此处引用程序需要的其他头文件
