#include "stdafx.h"
#define HOOK_CreateFile
#define HOOK_ReadFile

typedef enum SYELOG_FLAG
{
	SYELOG_SEVERITY_INFORMATION,
	SYELOG_SEVERITY_NOTICE
};

VOID SyelogExV(BOOL fTerminate, BYTE nSeverity, PCSTR pszMsgf, va_list args)
{
	(void)fTerminate;

	CHAR szBuffer[1024];
	PCHAR psz = szBuffer;
	BOOL fLf = FALSE;

	StringCchPrintfA(psz, szBuffer + sizeof(szBuffer)-psz, "--.%02x: ", nSeverity);
	while (*psz) {
		psz++;
	}

	StringCchVPrintfA(psz, szBuffer + sizeof(szBuffer)-psz, pszMsgf, args);
	for (psz = szBuffer; *psz; psz++) {
		if (*psz == '\n') {
			if (fLf) {
				*psz = '\0';
				break;
			}
			fLf = TRUE;
		}
	}
	if (!fLf) {
		*psz++ = '\n';
		*psz = '\0';
	}
	printf("%s", szBuffer);
	OutputDebugStringA(szBuffer);
}

VOID SyelogV(BYTE nSeverity, PCSTR pszMsgf, va_list args)
{
	SyelogExV(FALSE, nSeverity, pszMsgf, args);
}

VOID Syelog(BYTE nSeverity, PCSTR pszMsgf, ...)
{
	va_list args;
	va_start(args, pszMsgf);
	SyelogExV(FALSE, nSeverity, pszMsgf, args);
	va_end(args);
}


#ifdef HOOK_CreateFile
HANDLE (WINAPI* Org_CreateFileW)(
	__in LPCWSTR lpFileName,
	__in DWORD dwDesiredAccess,
	__in DWORD dwShareMode,
	__in_opt LPSECURITY_ATTRIBUTES lpSecurityAttributes,
	__in DWORD dwCreationDisposition,
	__in DWORD dwFlagsAndAttributes,
	__in_opt HANDLE hTemplateFile) = CreateFileW;

HANDLE(WINAPI* Org_CreateFileA)(
	_In_ LPCSTR lpFileName,
	_In_ DWORD dwDesiredAccess,
	_In_ DWORD dwShareMode,
	_In_opt_ LPSECURITY_ATTRIBUTES lpSecurityAttributes,
	_In_ DWORD dwCreationDisposition,
	_In_ DWORD dwFlagsAndAttributes,
	_In_opt_ HANDLE hTemplateFile
	) = CreateFileA;

HANDLE WINAPI Trap_CreateFileW(
	__in LPCWSTR lpFileName,
	__in DWORD dwDesiredAccess,
	__in DWORD dwShareMode,
	__in_opt LPSECURITY_ATTRIBUTES lpSecurityAttributes,
	__in DWORD dwCreationDisposition,
	__in DWORD dwFlagsAndAttributes,
	__in_opt HANDLE hTemplateFile
	)
{
	//DoSomething
	WCHAR FileName[MAX_PATH];
	wcsncpy(FileName, lpFileName, MAX_PATH);
	if (wcsstr(FileName, L"dalvik.ivd") != NULL)
	{
		//MSGBOXBREAK("CreateFile","CreateFile");
	}
	return Org_CreateFileW(
		lpFileName,
		dwDesiredAccess,
		dwShareMode,
		lpSecurityAttributes,
		dwCreationDisposition,
		dwFlagsAndAttributes,
		hTemplateFile);
}
#endif // DEBUG

#ifdef HOOK_ReadFile

BOOL (WINAPI* Org_ReadFile)(
	_In_        HANDLE       hFile,
	_Out_       LPVOID       lpBuffer,
	_In_        DWORD        nNumberOfBytesToRead,
	_Out_opt_   LPDWORD      lpNumberOfBytesRead,
	_Inout_opt_ LPOVERLAPPED lpOverlapped
	) = ReadFile;


BOOL WINAPI Trap_ReadFile(
	_In_        HANDLE       hFile,
	_Out_       LPVOID       lpBuffer,
	_In_        DWORD        nNumberOfBytesToRead,
	_Out_opt_   LPDWORD      lpNumberOfBytesRead,
	_Inout_opt_ LPOVERLAPPED lpOverlapped
	)
{
	//DoSomething
	WCHAR FileName[MAX_PATH];
	GetFinalPathNameByHandle(hFile, FileName, MAX_PATH, VOLUME_NAME_NT);
	if (wcsstr(FileName, L"dalvik.ivd") != NULL)
	{
		LARGE_INTEGER MoveDistance;
		MoveDistance.QuadPart = 0;
		SetFilePointerEx(hFile, MoveDistance, &MoveDistance, FILE_CURRENT);
		printf("ReadDistance: %X  ReadNumber:%X \r\n", MoveDistance.LowPart, nNumberOfBytesToRead);
		MSGBOXBREAK("Read", "Read");
	}
	return Org_ReadFile(
		hFile,
		lpBuffer,
		nNumberOfBytesToRead,
		lpNumberOfBytesRead,
		lpOverlapped
		);
}
#endif

static PCHAR DetRealName(PCHAR psz)
{
	PCHAR pszBeg = psz;
	// Move to end of name.
	while (*psz) {
		psz++;
	}
	// Move back through A-Za-z0-9 names.
	while (psz > pszBeg &&
		((psz[-1] >= 'A' && psz[-1] <= 'Z') ||
		(psz[-1] >= 'a' && psz[-1] <= 'z') ||
		(psz[-1] >= '0' && psz[-1] <= '9'))) {
		psz--;
	}
	return psz;
}

static VOID Dump(PBYTE pbBytes, LONG nBytes, PBYTE pbTarget)
{
	CHAR szBuffer[256];
	PCHAR pszBuffer = szBuffer;

	for (LONG n = 0; n < nBytes; n += 12) {
		pszBuffer += StringCchPrintfA(pszBuffer, sizeof(szBuffer), "  %p: ", pbBytes + n);
		for (LONG m = n; m < n + 12; m++) {
			if (m >= nBytes) {
				pszBuffer += StringCchPrintfA(pszBuffer, sizeof(szBuffer), "   ");
			}
			else {
				pszBuffer += StringCchPrintfA(pszBuffer, sizeof(szBuffer), "%02x ", pbBytes[m]);
			}
		}
		if (n == 0) {
			pszBuffer += StringCchPrintfA(pszBuffer, sizeof(szBuffer), "[%p]", pbTarget);
		}
		pszBuffer += StringCchPrintfA(pszBuffer, sizeof(szBuffer), "\n");
	}

	Syelog(SYELOG_SEVERITY_INFORMATION, "%s", szBuffer);
}

static VOID Decode(PBYTE pbCode, LONG nInst)
{
	PBYTE pbSrc = pbCode;
	PBYTE pbEnd;
	PBYTE pbTarget;
	for (LONG n = 0; n < nInst; n++) {
		pbTarget = NULL;
		pbEnd = (PBYTE)DetourCopyInstruction(NULL, (PVOID)pbSrc, (PVOID*)&pbTarget);
		Dump(pbSrc, (int)(pbEnd - pbSrc), pbTarget);
		pbSrc = pbEnd;

		if (pbTarget != NULL) {
			break;
		}
	}
}

VOID DetAttach(PVOID *ppvReal, PVOID pvMine, PCHAR psz)
{
	PVOID pvReal = NULL;
	if (ppvReal == NULL) {
		ppvReal = &pvReal;
	}

	LONG l = DetourAttach(ppvReal, pvMine);
	if (l != 0) {
		Syelog(SYELOG_SEVERITY_NOTICE,
			"Attach failed: `%s': error %d\n", DetRealName(psz), l);

		Decode((PBYTE)*ppvReal, 3);
	}
}

VOID DetDetach(PVOID *ppvReal, PVOID pvMine, PCHAR psz)
{
	LONG l = DetourDetach(ppvReal, pvMine);
	if (l != 0) {
#if 0
		Syelog(SYELOG_SEVERITY_NOTICE,
			"Detach failed: `%s': error %d\n", DetRealName(psz), l);
#else
		(void)psz;
#endif
	}
}

#define ATTACH(x)       DetAttach(&(PVOID&)Org_##x,Trap_##x,#x)
#define DETACH(x)       DetDetach(&(PVOID&)Org_##x,Trap_##x,#x)

DWORD DetourAttachWINAPI()
{
	DetourTransactionBegin();
#ifdef HOOK_CreateFile
	ATTACH(CreateFileW);
#endif
#ifdef HOOK_ReadFile
	ATTACH(ReadFile);
#endif
	DetourTransactionCommit();
}

DWORD DetourDetach()
{
	DetourTransactionBegin();
#ifdef HOOK_CreateFile
	DETACH(CreateFileW);
#endif
#ifdef HOOK_ReadFile
	DETACH(ReadFile);
#endif
	DetourTransactionCommit();
}